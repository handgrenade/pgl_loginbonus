#==============================================================================
defmodule PGS.LoginBonus.Daily do
    @moduledoc """
    Library for creating a Daily Login bonus for players
    """

    #==========================================================================
    @doc """
    Process the session to see if a daily login is availble.  If so it will
    send a mail message to the player with the login info needed.  This message
    will expire by the end of the day 
    """ 
    def process() do
        preDaily = Sys.Player.Session.dailyCount
        Sys.Player.Session.update
        bonusReady = false 
        postDaily = Sys.Player.Session.dailyCount
        # if this is the first time the player has logged in today
        if 1 == Sys.Player.Session.dailyCount do
            bonusReady = true
            # Send them a mail message that can be executed
            expires = Sys.Player.Session.timeToNextDay()
            Sys.Player.mailSend(Sys.Player.pKey, 
                                "_daily_login",
                                "_daily_login",
                                %{ "day" => Sys.Calendar.daysFromEpoch },
                                ["hidden", "cmd", "lib_login_daily"],
                                expires)
        end 
        bonusReady
    end
    
    #==========================================================================
    @doc """
    Trys to do a claim on the daily login bonus.  If the day is not
    """ 
    @spec claim() :: true | false

    def claim() do
        Sys.Player.Session.update
        last_day    = Sys.Player.localProperty(:_daily_login_last, 0)
        login_day   = Sys.Calendar.daysFromEpoch
        if login_day > last_day do
            Sys.Player.setLocalProperty(:_daily_login_last, login_day)
            true
        else
            false
        end
    end

    #==========================================================================
    @doc """
    Trys to do a claim for the daily login bonus based on the info returned
    from a system mail sent to claim the bonus.
    """ 
    def claim(data) when is_map(data) do
        Sys.Player.Session.update
        last_day    = Sys.Player.localProperty(:_daily_login_last, 0)
        login_day   = Dict.get(data, "day", -1)
        if login_day > last_day do
            Sys.Player.setLocalProperty(:_daily_login_last, login_day)
            true
        else
            false
        end
    end
    
    #==========================================================================
    @doc """
    Trys to claim a login bonus based on a day.  This is useful if you need a
    system where loggin will give you a token but the token does not expire
    at the end of the day.
    """ 
    def claim(login_day) when is_integer(login_day) do
        Sys.Player.Session.update
        last_day    = Sys.Player.localProperty(:_daily_login_last, 0)
        if login_day > last_day do
            Sys.Player.setLocalProperty(:_daily_login_last, login_day)
            true
        else
            false
        end
    end
    def claim(_), do: false
    

end
